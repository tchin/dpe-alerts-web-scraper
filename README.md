# Data Engineering Alerts Dashboard

## Generate data to json file

1. Make a `.env` file and fill in details:
```
EMAIL_USERNAME=
EMAIL_PASSWORD=
OUTPUT=json
```

2. Add a start page and how many pages to process:
```
START_PAGE=
NUM_PAGES=
```

3. 
```bash
npm run build
node dist/index.js
```

4. Move the `data.json` file into `server/utils` of the [dashboard repo](https://gitlab.wikimedia.org/tchin/dpe-alerts-dashboard)

## Generate data into MariaDB

1. Make a `.env` file and fill in details:
```
EMAIL_USERNAME=
EMAIL_PASSWORD=
SQL_DATABASE=
SQL_USER=
SQL_PASSWORD=
SQL_HOST=
SQL_PORT=
STORE=sql
```

2. Add a start page and how many pages to process:
```
START_PAGE=
NUM_PAGES=
```

3. 
```bash
npm run build
node dist/index.js
```

## Deploy on Toolforge

1. Build this repo as an image in Toolforge
```bash
toolforge build start https://gitlab.wikimedia.org/tchin/dpe-alerts-web-scraper
```

2. Set env vars using `toolforge envvars`
https://wikitech.wikimedia.org/wiki/Help:Toolforge/Envvars_Service

3. Run image as a one-off job:
```bash
toolforge jobs run --image tool-dpe-alerts-dashboard/tool-dpe-alerts-dashboard:latest --command "scrape" test-scrape
```
or
```bash
toolforge jobs run --image tool-dpe-alerts-dashboard/tool-dpe-alerts-dashboard:latest --command "sh -c 'npm run install:chrome; npm run scrape'" test-scrape
```

See logs while the job is running:
```bash
toolforge jobs logs test-scrape -f
```

Can also run it as a scheduled job:
```bash
toolforge jobs run --image tool-dpe-alerts-dashboard/tool-dpe-alerts-dashboard:latest --command "scrape" hourly-scrape --schedule "@hourly"
```

You might need to increase the memory of the job with something like `--men 2Gi` to the job if it encounters a page with an unprecedented about of text.