import "dotenv/config";

import playwright from "playwright";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat.js";
dayjs.extend(customParseFormat);

import ThreadProcessor from "./thread-processor";
import Store from "./store/store";
import { JSONStore } from "./store/json-store";
import { SQLStore } from "./store/sql-store";

(async () => {
  if (!process.env.EMAIL_USERNAME || !process.env.EMAIL_PASSWORD) {
    throw new Error("No email credentials provided.");
  }

  if (process.env.TOOL_DATABASE_USER && process.env.TOOL_DATABASE_PASSWORD) {
    console.log("Using provided ToolsDB credentials");
    process.env.SQL_USERNAME = process.env.TOOL_DATABASE_USER;
    process.env.SQL_PASSWORD = process.env.TOOL_DATABASE_PASSWORD;
  }

  let dataStore: Store;

  if (process.env.SQL_HOST && process.env.STORE == "sql") {
    console.log("Connecting to SQL Database");
    dataStore = new SQLStore({
      username: process.env.SQL_USERNAME,
      password: process.env.SQL_PASSWORD,
      host: process.env.SQL_HOST,
      port: parseInt(process.env.SQL_PORT || "3306"),
      database: process.env.SQL_DATABASE,
      dialect: "mysql", // We're using mysql2 as a driver even though we're using mariadb
      logging: false,
    });
  } else {
    console.log("Outputting to data.json");
    dataStore = new JSONStore();
  }

  await dataStore.init();

  if (!dataStore.testConnection()) {
    return;
  }

  const browser = await playwright.chromium.launch({
    headless: true,
    timeout: 120000,
  });
  const page = await browser.newPage();

  await page.goto(
    "https://lists.wikimedia.org/accounts/login/?next=/postorius/lists/data-engineering-alerts.lists.wikimedia.org/"
  );

  await page.setViewportSize({ width: 1080, height: 1024 });

  await page.fill('input[id="id_login"]', process.env.EMAIL_USERNAME);
  await page.fill('input[id="id_password"]', process.env.EMAIL_PASSWORD);

  await page.click('.login button.btn-primary[type="submit"]');
  await page.waitForURL("**/*");

  await page.goto(
    "https://lists.wikimedia.org/hyperkitty/list/data-engineering-alerts@lists.wikimedia.org/latest?page=1",
    { timeout: 120000 }
  );

  const threadProcessor = new ThreadProcessor();

  const pageLinks = await page.$$("a.page-link");
  let lastPage = await pageLinks[pageLinks.length - 2].evaluate((el) =>
    parseInt(el.textContent || "1")
  );

  const startPageEnv = process.env.START_PAGE;
  const startPageNum = parseInt(startPageEnv, 10);

  // Checking if START_PAGE is a positive number
  // If it's greater than lastPage, default to lastPage
  // If START_PAGE is not defined or not a valid number, default to 1
  const firstPage =
    !startPageEnv || isNaN(startPageNum) || startPageNum <= 0
      ? 1
      : startPageNum > lastPage
      ? lastPage
      : startPageNum;

  // Sets lastPage as being the amount to process or the last page of archives
  lastPage = Math.min(
    firstPage + (parseInt(process.env.NUM_PAGES) || 0),
    lastPage
  );
  console.log(`Processing from ${firstPage} to ${lastPage}`);

  for (let i = firstPage; i <= lastPage; i++) {
    console.log(`Processing Page: ${i}`);
    await page.goto(
      `https://lists.wikimedia.org/hyperkitty/list/data-engineering-alerts@lists.wikimedia.org/latest?page=${i}`,
      { timeout: 120000 }
    );
    const threads = await threadProcessor.getThreadsDataFromPage(page);
    console.log(`Page Time: ${threads[0].estimatedPostDate}`);
    dataStore.insertThreads(threads);
  }

  await browser.close();
  await dataStore.close();
})();
