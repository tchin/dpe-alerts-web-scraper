import NagiosParser from "./nagios-parser";
import { Thread } from "./thread-parser";
const nagiosParser = new NagiosParser();

const createNagiosThread = (thread: Partial<Thread>) => {
    return {
        title: "Refine failures for job refine_eventlogging_analytics",
        threadId: "test",
        tags: [],
        estimatedPostDate: "",
        lastReplyDate: "",
        author: "nagios",
        ...thread
    }
}

test("Recoveries have no severity", () => {
  expect(
    nagiosParser.parse(createNagiosThread({
        title: "** RECOVERY alert - an-worker1119/Hadoop NodeManager is OK **",
        details: `
        Notification Type: RECOVERY

Service: Hadoop NodeManager
Host: an-worker1119
Address: 10.64.5.8
State: OK
        `
    })).severity
  ).toBe("none");
});

test("Acknowledgements have no severity", () => {
    expect(
      nagiosParser.parse(createNagiosThread({
          title: "** ACKNOWLEDGEMENT alert - an-presto1008/Presto Server is CRITICAL **",
          details: `
          Notification Type: ACKNOWLEDGEMENT

          Service: Presto Server
          Host: an-presto1008
          Address: 10.64.139.3
          State: CRITICAL      
          `
      })).severity
    ).toBe("none");
  });

test("hadoop system is labelled", () => {
  expect(
    nagiosParser.parse(createNagiosThread({
        title: "** RECOVERY alert - an-worker1119/Hadoop NodeManager is OK **",
        details: `
        Notification Type: RECOVERY

        Service: Hadoop NodeManager
        Host: an-worker1119
        Address: 10.64.5.8
        State: OK
        `
    })).system
  ).toBe("hadoop");
});

test("druid system is labelled", () => {
    expect(
      nagiosParser.parse(createNagiosThread({
          title: "** PROBLEM alert - druid1009/Disk space is CRITICAL **",
          details: `
          Notification Type: PROBLEM

          Service: Disk space
          Host: druid1009
          Address: 10.64.131.9
          State: CRITICAL          
          `
      })).system
    ).toBe("druid");
  });

test("kafka system is labelled", () => {
    expect(
      nagiosParser.parse(createNagiosThread({
          title: "** PROBLEM alert - kafka-jumbo1015/Kafka Broker Server #page is CRITICAL **",
          details: `
          Notification Type: PROBLEM

          Service: Kafka Broker Server #page
          Host: kafka-jumbo1015
          Address: 10.64.136.11
          State: CRITICAL       
          `
      })).system
    ).toBe("kafka");
  });

test("presto system is labelled", () => {
    expect(
      nagiosParser.parse(createNagiosThread({
          title: "** ACKNOWLEDGEMENT alert - an-presto1008/Presto Server is CRITICAL **",
          details: `
          Notification Type: ACKNOWLEDGEMENT

          Service: Presto Server
          Host: an-presto1008
          Address: 10.64.139.3
          State: CRITICAL      
          `
      })).system
    ).toBe("presto");
  });