import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat.js";
import utc from "dayjs/plugin/utc";
dayjs.extend(customParseFormat);
dayjs.extend(utc);

import { Thread, ThreadParser } from "./thread-parser";

export default class NagiosParser extends ThreadParser {
  isParseable(thread: Thread): boolean {
    return thread.author.includes("nagios");
  }

  parse(thread: Thread): Thread {
    let parsedData: Partial<Thread> = {
      system: "nagios",
      severity: "high",
      tags: thread.tags,
    };

    const typeMatch = thread.details.match(/Notification Type:\s*(.*)/);
    const serviceMatch = thread.details.match(/Service:\s*(.*)/);
    const stateMatch = thread.details.match(/State:\s*(.*)/);
    const dateMatch = thread.details.match(/Date\/Time:\s*(.*)/);

    if (typeMatch) {
      parsedData.tags.push(typeMatch[1]);
      if (typeMatch[1] == "RECOVERY" || typeMatch[1] == "ACKNOWLEDGEMENT") {
        parsedData.severity = "none";
      }
    }
    if (serviceMatch) {
      parsedData.tags.push(serviceMatch[1]);
    }
    if (stateMatch) {
      parsedData.tags.push(stateMatch[1]);
    }
    if (dateMatch) {
      parsedData.estimatedPostDate = dayjs(dateMatch[1]).toISOString();
    }

    const threadTitle = thread.title.toLowerCase()
    if (threadTitle.includes("hadoop")) {
      parsedData.system = "hadoop";
    } else if (threadTitle.includes("druid")) {
      parsedData.system = "druid";
    } else if (threadTitle.includes("kafka")) {
      parsedData.system = "kafka";
    } else if (threadTitle.includes("presto")) {
      parsedData.system = "presto";
    } else if (threadTitle.includes("systemd")) {
      parsedData.system = "systemd";
    } else if (threadTitle.includes("flink")) {
      parsedData.system = "flink";
    } else if (threadTitle.includes("aqs")) {
      parsedData.system = "aqs";
    } else if (threadTitle.includes("airflow")) {
      parsedData.system = "airflow";
    } else if (threadTitle.includes("matomo")) {
      parsedData.system = "matomo";
    }

    return {
      ...thread,
      ...parsedData,
    };
  }
}
