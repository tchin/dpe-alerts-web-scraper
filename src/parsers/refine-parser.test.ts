import RefineParser from "./refine-parser";
const refineParser = new RefineParser();

test("Parses job tag correctly", () => {
  expect(
    refineParser.parse({
      title: "RefineMonitor problem report for job monitor_refine_event",
      threadId: "test",
      tags: [],
      estimatedPostDate: "",
      lastReplyDate: "",
      author: "refine",
    }).tags[0]
  ).toBe("monitor_refine_event");
  expect(
    refineParser.parse({
      title: "Refine failures for job refine_eventlogging_analytics",
      threadId: "test",
      tags: [],
      estimatedPostDate: "",
      lastReplyDate: "",
      author: "refine",
    }).tags[0]
  ).toBe("refine_eventlogging_analytics");
});
