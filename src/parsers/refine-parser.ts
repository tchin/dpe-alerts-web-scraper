import { Thread, ThreadParser } from "./thread-parser";

export default class RefineParser extends ThreadParser {
  isParseable(thread: Thread): boolean {
    return thread.author.includes("refine");
  }

  parse(thread: Thread): Thread {
    let parsedData: Partial<Thread> = {
      system: "refine",
      severity: "high",
      tags: thread.tags,
    };

    const jobMatch = thread.title.match(/for job (\S+)/);

    if (jobMatch) {
      parsedData.tags.push(jobMatch[1]);
    }

    return {
      ...thread,
      ...parsedData,
    };
  }
}
