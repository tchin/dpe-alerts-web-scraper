import RelengParser from "./releng-parser";
const relengParser = new RelengParser();

test("Parses build tag correctly", () => {
  expect(
    relengParser.parse({
      title: "Update on build jenkins-analytics-refinery-update-jars-docker-89",
      threadId: "test",
      tags: [],
      estimatedPostDate: "",
      lastReplyDate: "",
      author: "Release Engineering",
    }).tags[0]
  ).toBe("jenkins-analytics-refinery-update-jars-docker-89");
});
