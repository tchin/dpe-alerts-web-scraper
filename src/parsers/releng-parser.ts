import { Thread, ThreadParser } from "./thread-parser";

export default class RelengParser extends ThreadParser {
  isParseable(thread: Thread): boolean {
    return thread.author.includes("Release Engineering");
  }

  parse(thread: Thread): Thread {
    let parsedData: Partial<Thread> = {
      system: "Release Engineering",
      severity: "none",
      tags: thread.tags,
    };

    const buildMatch = thread.title.match(/Update on build (\S+)/);

    if (buildMatch) {
      parsedData.tags.push(buildMatch[1]);
    }

    return {
      ...thread,
      ...parsedData,
    };
  }
}
