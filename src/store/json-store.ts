import * as fs from "node:fs/promises";
import { Thread } from "../parsers/thread-parser";
import Store from "./store";

export class JSONStore implements Store {
  private json: { data: Thread[] };
  constructor() {
    this.json = { data: [] };
  }
  async init() {
    return;
  }
  async close() {
    console.log("Writing to file...");
    await fs.writeFile("data.json", JSON.stringify(this.json), "utf8");
  }
  async testConnection() {
      return true;
  }
  async insertThreads(threads: Thread[]) {
    this.json.data.push(...threads);
  }
}
