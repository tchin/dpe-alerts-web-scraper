-- No need to run these manually. The scraper will create the tables if they don't exist.

-- This is not very performant and will need to be redesigned if
-- this MVP ends up existing longer than we'd like
CREATE TABLE IF NOT EXISTS threads (
    title VARCHAR(255),
    thread_id VARCHAR(255) PRIMARY KEY,
    details TEXT,
    estimated_post_date DATETIME, -- This is unreliable right now. Use last_reply_date
    last_reply_date DATETIME,
    author VARCHAR(255),
    data_system VARCHAR(255),
    severity VARCHAR(255),
    tags TEXT -- comma-separated string
);
