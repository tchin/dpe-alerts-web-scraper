import { Thread } from "../parsers/thread-parser";
import {
  Sequelize,
  DataTypes,
  Options as SequelizeOptions,
  Model,
  ModelStatic,
} from "sequelize";
import Store from "./store";

const createThreadModel = (sequelize: Sequelize) => {
  return sequelize.define(
    "Thread",
    {
      thread_id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      title: {
        type: DataTypes.STRING(1024), // Some titles are extremely long
        allowNull: false,
      },
      author: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      details: {
        type: DataTypes.TEXT,
        defaultValue: "",
      },
      estimated_post_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      last_reply_date: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      data_system: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "unknown",
      },
      severity: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "low",
      },
      tags: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
    },
    {
      tableName: "threads",
      indexes: [
        {
          type: "FULLTEXT",
          fields: ["title"]
        }
      ]
    }
  );
};

export class SQLStore implements Store {
  private sequelize: Sequelize;
  private threadModel: ModelStatic<Model>;
  constructor(options: SequelizeOptions) {
    this.sequelize = new Sequelize(options);
    this.threadModel = createThreadModel(this.sequelize);
  }

  async init() {
    this.threadModel.sync({ alter: true });
  }

  async close() {
    this.sequelize.close();
  }

  async testConnection() {
    try {
      await this.sequelize.authenticate();
      console.log("Connection has been established successfully.");
      return true;
    } catch (error) {
      console.error("Unable to connect to the database:", error);
      return false;
    }
  }

  async insertThreads(threads: Thread[]) {
    await this.sequelize.transaction(async (t) => {
      const _threads = threads.map((thread) => ({
        thread_id: thread.threadId,
        title: thread.title,
        author: thread.author,
        details: thread.details || "",
        estimated_post_date: new Date(thread.estimatedPostDate),
        last_reply_date: new Date(thread.lastReplyDate),
        data_system: thread.system,
        severity: thread.severity,
        tags: thread.tags.join(","),
      }));
      this.threadModel.bulkCreate(_threads, {
        updateOnDuplicate: ["data_system", "severity", "tags"],
      });
    });
  }
}
