import { Thread } from "../parsers/thread-parser";

export default abstract class Store {
    abstract init(): Promise<void>
    abstract close(): Promise<void>
    abstract testConnection(): Promise<boolean>
    abstract insertThreads(threads: Thread[]): Promise<void>
}