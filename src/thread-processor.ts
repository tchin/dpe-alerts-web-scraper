import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import playwright from "playwright";

import { Thread, ThreadParser } from "./parsers/thread-parser";
import AirflowParser from "./parsers/airflow-parser";
import NagiosParser from "./parsers/nagios-parser";
import SystemdtimerParser from "./parsers/systemdtimer-parser";
import PrometheusParser from "./parsers/prometheus-parser";
import RefineParser from "./parsers/refine-parser";
import RelengParser from "./parsers/releng-parser";
dayjs.extend(customParseFormat);

const parsers: ThreadParser[] = [
  new AirflowParser(),
  new NagiosParser(),
  new SystemdtimerParser(),
  new PrometheusParser(),
  new RefineParser(),
  new RelengParser()
];

export default class ThreadProcessor {
  _parseDateStringToISOString(str: string): string {
    const date = str.split(", ")[1];
    const d = dayjs(date, "D MMMM YYYY HH:mm:ss");
    return d.toISOString();
  }

  async getThreadsDataFromPage(page: playwright.Page) {
    const threadData: Thread[] = [];
    const threads = await page.$$(".thread");
    for (let i = 0; i < threads.length; i++) {
      let data: Thread = {
        title: "",
        threadId: "",
        estimatedPostDate: "",
        lastReplyDate: "",
        author: "",
        details: "",
        tags: [],
      };
      const thread = threads[i];
      const titleEl = await thread.$(".thread-title");
      data.title = (await titleEl?.evaluate((el) => el.textContent)).trim();
      data.threadId = await titleEl.evaluate((el) => el.getAttribute("name"));
      const dateEl = await thread.$(".thread-date");
      data.lastReplyDate = this._parseDateStringToISOString(
        await dateEl?.evaluate((el) => el.getAttribute("title"))
      );
      data.estimatedPostDate = data.lastReplyDate;
      const authorEl = await thread.$(".thread-title+div");
      data.author =
        (await authorEl?.evaluate((el) =>
          el.textContent.split("by ")[1].trim()
        )) || "";
      const detailsEl = await thread.$("span.expander");
      data.details = await detailsEl?.evaluate((el) => el.textContent);

      for (const parser of parsers) {
        if (parser.isParseable(data)) {
          data = parser.parse(data);
          break;
        }
      }

      // data.details might have an excessive amount of text that we don't want to save
      data.details = data.details.substring(0, 500);

      // Set defaults
      data.system ??= "unknown";

      threadData.push(data);
    }
    return threadData;
  }
}
